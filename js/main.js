window.onload = function(){

	var signup = $("#signup");
	var parallax = $(".parallax");

	var body = document.body;
	var signupClouds = document.querySelector("#signup .illustration");
	var chara1 = document.getElementById("chara-1");
	var pageCurrent;

	// TweenMax.set(signupClouds, {
	// 	z: "0"
	// })

	function getBrowserName() {
    var name = "Unknown";
    if(navigator.userAgent.indexOf("MSIE")!=-1){
        name = "MSIE";
    }
    else if(navigator.userAgent.indexOf("Firefox")!=-1){
        name = "Firefox";
    }
    else if(navigator.userAgent.indexOf("Opera")!=-1){
        name = "Opera";
    }
    else if(navigator.userAgent.indexOf("Chrome") != -1){
        name = "Chrome";
    }
    else if(navigator.userAgent.indexOf("Safari")!=-1){
        name = "Safari";
    }
    return name;   
	}

	var cloudsGap = 600;

	var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

	// scroll to
	var buttons = document.querySelectorAll(".go-to-form");
	buttons = [].slice.call(buttons);

	buttons.forEach(function(button){

		button.addEventListener("click", function(e){

			goToSignupButton.click();

		});

	});



	// email interaction
	var form = document.getElementById("signup-form"),
			inputEmail = document.getElementById("mce-EMAIL");

	form.onsubmit = function(e){

		if(!inputEmail.classList.contains("success")) {

			e.preventDefault();

		} else {

			formDetail.textContent = "Thanks for subscribing!";
			inputEmail.blur();
			inputEmail.className = "";

		}

	}

	inputEmail.addEventListener("keyup", function(e){

		if(validateEmail(this.value)){
		  this.className = "success";
		} else {
		  this.className = "error";
		}
		
	});

	function validateEmail(email) {
	  var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	  return regex.test(email);
	}



	// if browser is not safari or browser is mobile
	if(getBrowserName() !== "Safari" || isMobile) {

		// characters animation
		// whip dude
		var whip = {
			right_hand: ".whip-dude #right-hand-wrapper",
			left_hand: ".whip-dude #left-hand",
			right_eye: ".whip-dude #right-eye",
			right_eye2: ".whip-dude #right-eye-angry",
			right_eye_ori: "",
			left_eye: ".whip-dude #left-eye",
			left_eye2: ".whip-dude #left-eye-angry",
			tail: ".whip-dude #tail"
		}

		var tlWhip = new TimelineMax({
			onComplete: function(){
				setTimeout(function(){
					tlWhip.restart();
				}, 600)
			}
		});

		// tlWhip.stop();

		tlWhip.set(whip.right_hand, {
			transformOrigin: "15% 15%"
		}).set(whip.left_hand, {
			transformOrigin: "15% 15%"
		}).set(whip.tail, {
			transformOrigin: "25% 10%"
		})

		// animation
		tlWhip.to(whip.right_hand, .6, {
			delay: .3,
			rotation: -45
		})

		tlWhip.to(whip.tail, .6, {
			rotation: 40
		}, .3)

		.to(whip.right_hand, .15, {
			delay: .3,
			rotation: 10
		})

		tlWhip.to(whip.tail, .3, {
			rotation: 380
		}, "-=.15")

		.to(whip.right_hand, .3, {
			rotation: 0
		})

		tlWhip.to(whip.tail, .3, {
			rotation: 360
		}, "-=.3")

		tlWhip.to(whip.left_hand, .6, {
			rotation: 0
		}, "-=0.6")




		// dick dude
		var dick = {
			hand: ".dick-dude #hand",
			hand2: "M94.7 173.3c39.8 52.4 157 78 242.2 93.4 61.3 11 68 70 60.5 84.3l-4.6 4",
			hand_ori: "M94.7 173.3c20.7 27.3 51.3 42.7 110 62.7 58.8 20 99.6 25.8 107.3 46 10.7 28-10 51-62 69",
			pants: ".dick-dude #pants",
			pants_ori: "M18.8 350.6S49 425 114 431c45 5 69-10 90-21 20-11.5 35.2-31.3 45.5-59.4H18.8z",
			pants2: "M18.8 350.6s16 51 70.6 75C134.4 445.3 192 441 233 433c54.4-10.6 128-38 162-82.4H18.8z",
			flashone: ".dick-dude #flash-1",
			flashtwo: ".dick-dude #flash-2"
		}

		var tlDick = new TimelineMax({
			onComplete: function(){
				setTimeout(function(){
					tlDick.restart();
				}, 600)
			}
		});

		// tlDick.stop();

		tlDick.set([dick.flashone, dick.flashtwo], {
			transformOrigin: "50% 50%",
			y: -4,
			scale: 0,
			rotation: 45
		}).set(dick.hand, {
			morphSVG: dick.hand_ori
		}).set(dick.pants, {
			morphSVG: dick.pants_ori
		})

		tlDick.to(dick.hand, .3, {
			delay: .3,
			morphSVG: dick.hand2
		})

		.to(dick.pants, .3, {
			morphSVG: dick.pants2,
		}, .3)

		.staggerTo([dick.flashone, dick.flashtwo], .15, {
			scale: 1.7,
			rotation: 0
		}, .15)

		.staggerTo([dick.flashone, dick.flashtwo], .15, {
			delay: .4,
			scale: 0,
			rotation: 45
		}, .15)

		.to(dick.pants, .2, {
			delay: .6,
			morphSVG: dick.pants_ori
		})

		.to(dick.hand, .3, {
			delay: .15,
			morphSVG: dick.hand_ori
		})




		// nickle girl
		var nickle = {
			head: "#nickel-girl #head",
			hand: "#nickel-girl #right-hand",
			hand2: "M92.4 340.6C26.4 277 0 273 0 245c0-33 81-2.7 91-37 1.5-5.3 1-14.5 0-20",
			hand3: "M92.4 340.6C26.4 277 28.4 246 34 230c10-28.6 69-38 79-51 6.7-8.7 9.7-17 9-25",
			hand_ori: "M92.4 340.6c-66-63.5-76.7-94-71-110 10-28.6 74.7-16 84.7-50.2 1.6-5.3 1.6-11 .6-16.4",
			melody1: "#nickel-girl #melody-1",
			melody2: "#nickel-girl #melody-2",
			melody3: "#nickel-girl #melody-3"
		}

		var melodies = [nickle.melody1, nickle.melody2, nickle.melody3];

		var tlNickle = new TimelineMax({
			onComplete: function(){
				setTimeout(function(){
					tlNickle.restart();
				}, 300)
			}
		});

		// tlNickle.stop();

		var tlMelodies = new TimelineMax({
			repeat: -1
		})

		// tlMelodies.stop();

		// set nicle
		tlNickle.set([nickle.head, nickle.hand], {
			transformOrigin: "center"
		})

		tlMelodies.set(melodies, {
			transformOrigin: "left bottom"
		})
		
		// animate nicle
		tlNickle.to(nickle.head, .45, {
			rotation: -10,
			x: -22,
			y: 4
		})

		.to(nickle.hand, .45, {
			morphSVG: nickle.hand2
		}, 0)

		.to(nickle.head, .45, {
			rotation: 10,
			x: 22,
			y: 4
		})

		.to(nickle.hand, .45, {
			morphSVG: nickle.hand3
		}, "-=0.45")

		.to(nickle.head, .3, {
			rotation: 0,
			x: 0,
			y: 0
		})

		.to(nickle.hand, .3, {
			morphSVG: nickle.hand_ori
		}, "-=0.3")

		
		// melodies
		tlMelodies

		.staggerTo(melodies, .6, {
			scale: 1.3,
			opacity: 1,
			ease: Elastic.easeOut.config(1, 0.35)
		}, .15)

		.staggerTo(melodies, .6, {
			scale: 1,
			ease: Elastic.easeOut.config(1, 0.35)
		}, .15, .6)





	} // end if not safari

	else { // if browser is safari

		var homeThirdbase = $("#home .third-base");
		var thirbaseSVGs = document.querySelectorAll("#home .chara");
		var thirbaseHeight;

		renderSVGHeight();

		$(window).bind("resize", function(){
			renderSVGHeight();
		});

		function renderSVGHeight(){
			thirbaseHeight = homeThirdbase.height();
			thirbaseSVGs.forEach(function(svg){
				svg.style.height = thirbaseHeight + "px";
			});

		}
	}




	// full page
	var fp = $("#fullpage").fullpage({
		verticalCentered: false,
		autoScrolling: true,
		navigation: true,
		'onLeave': function(index, nextIndex, direction){

			parallax.css("transition", "3s ease");

			// direction
			if(direction == 'down') {
				pageCurrent = parseInt(body.className.split("-")[2]) + 1;
			} else {
				pageCurrent = parseInt(body.className.split("-")[2]) - 1;
			}


			// parallax
			if(pageCurrent === 0) {

				parallax.css("transform", "translateY(" + 150 + "px)");

			} else if(pageCurrent === 1) {

				// signup.addClass("hide-clouds");
				// signupClouds.style.transform = "translateY(-100%)";
				// signupClouds.style.transition = ".9s all ease .45s";
				parallax.css("transform", "translateY(" + (pageCurrent * -cloudsGap) + "px)");

				// TweenMax.to(signupClouds, .9, {
				// 	delay: .45,
				// 	y: "-100%"
				// })

				anime({
					targets: signupClouds,
					duration: 450,
					delay: 300,
					translateY: "-100%",
					easing: 'easeInQuad'
				})

			} else if(pageCurrent === 2) {

				parallax.css("transform", "translateY(" + (pageCurrent * -cloudsGap) + "px)");
				// tlWhip.resume();
				// sections[pageCurrent].classList.add("animate-cloud-back");

			} else if(pageCurrent === 3) {

				parallax.css("transform", "translateY(" + (pageCurrent * -cloudsGap) + "px)");
				// tlNickle.resume();

			} else if(pageCurrent === 4) {

				parallax.css("transform", "translateY(" + (pageCurrent * -cloudsGap) + "px)");
				// tlDick.resume();

			} else if(pageCurrent === 5) {

				parallax.css("transform", "translateY(" + (pageCurrent * -cloudsGap) + "px)");

			}

			// // show clouds if user scrolls to home section
			if(index == 2 && direction == 'up'){
				// TweenMax.to(signupClouds, .3, {
				// 	y: "0%"
				// })

				anime({
					targets: signupClouds,
					duration: 200,
					translateY: "0%",
					easing: 'linear'
				})
			}

		},
		onSlideLeave: function(){
			console.log("whats this?");
		}
	});

	var goToSignupButton = document.querySelectorAll("#fp-nav li:nth-child(2) a")[0];

};



